/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul2.provide_mutable_classes_with_copy_functionallity_to_safety_allow_passing_instances_to_untrusted_code;

import java.util.Date;

/**
 *
 * @author Randi 133040229
 */
public class UnmodifiableDateView extends Date{
    private Date date;
    public UnmodifiableDateView(Date date) {
        this.date = date;
    }
    
    public void setTime(long date) {
        throw new UnsupportedOperationException();
    }
    
    // Override all other mutator methods to throw UnsupportedOperationException
}
