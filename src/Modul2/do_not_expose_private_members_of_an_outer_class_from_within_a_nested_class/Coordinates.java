/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul2.do_not_expose_private_members_of_an_outer_class_from_within_a_nested_class;

/**
 *
 * @author Randi 133040229
 */
public class Coordinates {
    private int x;
    private int y;
    // Noncompliant code
//    public class Point {
//        public void getPoint() {
//            System.out.println("(" + x + "," + y + ")");
//        }
//    }
    
    // Compliant code
    private class Point {
        private void getPoint() {
            System.out.println("(" + x + "," + y + ")");
        }
    }
}

