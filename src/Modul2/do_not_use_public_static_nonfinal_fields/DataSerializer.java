/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul2.do_not_use_public_static_nonfinal_fields;

import java.io.Serializable;

/**
 *
 * @author Randi 133040229
 */
public class DataSerializer implements Serializable{
    // Noncompliant code
//    public static long serialVersionUID = 1973473122623778747L;
    // ...
    
    // Compliant code
    private static final long serialVersionUID = 1973473122623778747L;
}
