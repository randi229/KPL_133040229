/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul3.follow_the_general_contract_when_implementing_the_compareto_method;

/**
 *
 * @author Randi 133040229
 */
// Noncompliant code
//class GameEntry implements Comparable {
//    public enum Roshambo {ROCK, PAPER, SCISSORS}
//    private Roshambo value;
//    
//    public GameEntry(Roshambo value) {
//        this.value = value;
//    }
//
//    public int compareTo(Object that) {
//        if (!(that instanceof GameEntry)) {
//            throw new ClassCastException();
//        }
//        GameEntry t = (GameEntry) that;
//        return (value == t.value) ? 0
//                : (value == Roshambo.ROCK && t.value == Roshambo.PAPER) ? -1
//                : (value == Roshambo.PAPER && t.value == Roshambo.SCISSORS) ? -1
//                : (value == Roshambo.SCISSORS && t.value == Roshambo.ROCK) ? -1
//                : 1;
//    }
//}

// Compliant code
class GameEntry{
    public enum Roshambo {ROCK, PAPER, SCISSORS}
    private Roshambo value;
    public GameEntry(Roshambo value) {
        this.value = value;
    }
    public int beats(Object that) {
        if (!(that instanceof GameEntry)) {
            throw new ClassCastException();
        }
        GameEntry t = (GameEntry) that;
        return (value == t.value) ? 0
                : (value == Roshambo.ROCK && t.value == Roshambo.PAPER) ? -1
                : (value == Roshambo.PAPER && t.value == Roshambo.SCISSORS) ? -1
                : (value == Roshambo.SCISSORS && t.value == Roshambo.ROCK) ? -1
                : 1;
    }
}
