/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul3.do_not_use_deprecated_or_obsolete_classes_or_methods;

/**
 *
 * @author Randi 133040229
 */
public final class Foo implements Runnable {
    @Override public void run() {
        System.out.println("Running...");
    }
    public static void main(String[] args) {
        Foo foo = new Foo();
        // Noncompliant code
//        new Thread(foo).run();
        // Compliant code
        new Thread(foo).start();
    }
}
