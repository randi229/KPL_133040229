/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul3.validate_method_arguments;

/**
 *
 * @author Randi 133040229
 */
public class latihan {
    // Noncompliant code
//    private Object myState = null;
//    // Sets some internal state in the library  
//    void setState(Object state) {
//        myState = state;
//    }
//    
//    // Performs some action using the state passed earlier
//    void useState() {
//         // Perform some action here
//    }
    
    // Compliant code
    private Object myState = null;
    // Sets some internal state in the library
    void setState(Object state) {
        if (state == null) {
            // Handle null state
        }
        // Defensive copy here when state is mutable
        if (isInvalidState(state)) {
            // Handle invalid state
        }
        myState = state;
    }
// Performs some action using the state passed earlier
void useState() {
 if (myState == null) {
 // Handle no state (e.g., null) condition
 }
 // ...
}

}
