/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul3.do_not_throw_undeclared_checked_exceptions;

/**
 *
 * @author Randi 133040229
 */
public class UndeclaredException {
    public static void main(String[] args) {
        // No declared checked exceptions
        NewInstance.undeclaredThrow(
        new Exception("Any checked exception"));
    }
//Class.netInstance Workarounds
// public static void main(String[] args) {
// try {
// NewInstance.undeclaredThrow(
// new IOException("Any checked exception"));
// } catch (Throwable e) {
// if (e instanceof IOException) {
// System.out.println("IOException occurred");
// } else if (e instanceof RuntimeException) {
// throw (RuntimeException) e;
// } else {
// // Forward to handler
// }
// }
}
