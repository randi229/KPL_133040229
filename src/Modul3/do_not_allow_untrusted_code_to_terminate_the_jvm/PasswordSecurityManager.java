/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul3.do_not_allow_untrusted_code_to_terminate_the_jvm;

/**
 *
 * @author Randi 133040229
 */
class PasswordSecurityManager extends SecurityManager {
    private boolean isExitAllowedFlag;
    
    public PasswordSecurityManager(){
        super();
        isExitAllowedFlag = false;
    }
    
    public boolean isExitAllowed(){
        return isExitAllowedFlag;
    }
    
    @Override
    public void checkExit(int status) {
        if (!isExitAllowed()) {
            throw new SecurityException();
        }
        super.checkExit(status);
    }

    public void setExitAllowed(boolean f) {
        isExitAllowedFlag = f;
    }
}
