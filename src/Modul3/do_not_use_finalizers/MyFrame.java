/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul3.do_not_use_finalizers;

import javax.swing.JFrame;

/**
 *
 * @author Randi 133040229
 */
// Noncompliant code
//class MyFrame extends JFrame {
//  private byte[] buffer = new byte[16 * 1024 * 1024];
//  // Persists for at least two GC cycles
//}

// Compliant code
class MyFrame {
  private JFrame frame;
  private byte[] buffer = new byte[16 * 1024 * 1024]; // Now decoupled
}