/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul3.do_not_use_finalizers;

/**
 *
 * @author Randi 133040229
 */
public class BaseClass {
    protected void finalize() throws Throwable {
    System.out.println("Superclass finalize!");
    doLogic();
  }
 
  public void doLogic() throws Throwable {
    System.out.println("This is super-class!");
  }
}
