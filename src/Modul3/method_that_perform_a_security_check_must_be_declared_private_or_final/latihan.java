/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul3.method_that_perform_a_security_check_must_be_declared_private_or_final;

/**
 *
 * @author Randi 133040229
 */
// Noncompliant code

class latihan{
    // Noncompliant code
//    public void readSensitiveFile() {
//        try {
//            SecurityManager sm = System.getSecurityManager();
//            if (sm != null) { // Check for permission to read file
//                sm.checkRead("/temp/tempFile");
//            }
//            // Access the file
//        } catch (SecurityException se) {
//        // Log exception
//        }
//    }
    
    // Compliant code 1
//    public final void readSensitiveFile() {
//        try {
//            SecurityManager sm = System.getSecurityManager();
//            if (sm != null) { // Check for permission to read file
//                sm.checkRead("/temp/tempFile");
//            }
//            // Access the file
//        } catch (SecurityException se) {
//            // Log exception
//        }
//    }
    
    // Compliant code 2
    private void readSensitiveFile() {
        try {
            SecurityManager sm = System.getSecurityManager();
            if (sm != null) { // Check for permission to read file
                sm.checkRead("/temp/tempFile");
            }
            // Access the file
        } catch (SecurityException se) {
            // Log exception
        }
    }
}