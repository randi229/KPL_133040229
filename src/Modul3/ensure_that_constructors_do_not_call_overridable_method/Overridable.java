/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul3.ensure_that_constructors_do_not_call_overridable_method;

/**
 *
 * @author Randi 133040229
 */
public class Overridable {
    public static void main(String[] args) {
        SuperClass bc = new SuperClass();
        // Prints "This is superclass!"
        SuperClass sc = new SubClass();
        // Prints "This is subclass! The color is :null"
    }
}
