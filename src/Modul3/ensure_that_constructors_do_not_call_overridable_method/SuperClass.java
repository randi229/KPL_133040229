/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul3.ensure_that_constructors_do_not_call_overridable_method;

/**
 *
 * @author Randi 133040229
 */
public class SuperClass {
    // Noncompliant code
//    public SuperClass () {
//        doLogic();
//    }
//    
//    public void doLogic() {
//        System.out.println("This is superclass!");
//    }
    
    // Compliant code
    public SuperClass() {
        doLogic();
    }
    public final void doLogic() {
        System.out.println("This is superclass!");
    }
}
