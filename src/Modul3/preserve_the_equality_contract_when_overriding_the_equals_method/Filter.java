/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul3.preserve_the_equality_contract_when_overriding_the_equals_method;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

/**
 *
 * @author Randi 133040229
 */
public class Filter {
    // Noncompliant code
//    public static void main(String[] args) throws MalformedURLException {
//        final URL allowed = new URL("http://mailwebsite.com");
//        if (!allowed.equals(new URL(args[0]))) {
//            throw new SecurityException("Access Denied");
//        }
//        // Else proceed
//    }
    
    // Compliant code 1 Strings
//    public static void main(String[] args) throws MalformedURLException {
//        final URL allowed = new URL("http://mailwebsite.com");
//        if (!allowed.toString().equals(new URL(args[0]).toString())) {
//            throw new SecurityException("Access Denied");
//        }
//        // Else proceed
//    }
    
    // compliant code 2 URI.equals()
    public static void main(String[] args) throws MalformedURLException, URISyntaxException {
        final URI allowed = new URI("http://mailwebsite.com");
        if (!allowed.equals(new URI(args[0]))) {
            throw new SecurityException("Access Denied");
        }
        // Else proceed
    }
}
