/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul1.prevent_class_initialization_cycles;

/**
 *
 * @author Randi 133040229
 */
public class Cycle {
    /* NonCompliant Code
    private final int balance;
    private static final Cycle c = new Cycle();
    private static final int deposit = (int) (Math.random() * 100);
    
    public Cycle(){
        balance = deposit - 10;
    }
    */
    
    // Compliant Code
    private final int balance;
    private static final int deposit = (int) (Math.random() * 100);
    private static final Cycle c = new Cycle();
    
    public Cycle(){
        balance = deposit - 10;
    }
    
    
    
    
    public static void main(String[] args) {
        System.out.println("The account balance is : "+c.balance);
    }
}
