/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.prevent_deseriliazation_of_untrusted_data;

/**
 *
 * @author Randi 133040229
 */
import java.io.*;
import java.util.*;

// Compliant code
class WhitelistedObjectInputStream extends ObjectInputStream {

    public Set whitelist;

    public WhitelistedObjectInputStream(InputStream inputStream, Set wl) throws IOException {
        super(inputStream);
        whitelist = wl;
    }

    @Override
    protected Class<?> resolveClass(ObjectStreamClass cls) throws IOException, ClassNotFoundException {
        if (!whitelist.contains(cls.getName())) {
            throw new InvalidClassException("Unexpected serialized class",
                    cls.getName());
        }
        return super.resolveClass(cls);
    }
}
