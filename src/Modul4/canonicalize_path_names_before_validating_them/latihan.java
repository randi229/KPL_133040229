/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.canonicalize_path_names_before_validating_them;

import java.io.File;
import java.io.FileInputStream;

/**
 *
 * @author Randi 133040229
 */
public class latihan {

    public static void main(String[] args) {
        // Noncompliant code
//        File file = new File("/img/" + args[0]);
//        if (!isInSecureDir(file)) {
//            throw new IllegalArgumentException();
//        }
//        String canonicalPath = file.getCanonicalPath();
//        FileOutputStream fis = new FileOutputStream(canonicalPath);
        // Compliant code
        File file = new File("/img/" + args[0]);
        if (!isInSecureDir(file)) {
            throw new IllegalArgumentException();
        }
        String canonicalPath = file.getCanonicalPath();
        if (!canonicalPath.equals("/img/java/file1.txt")
                && !canonicalPath.equals("/img/java/file2.txt")) {
// Invalid file; handle error
        }
        FileInputStream fis = new FileInputStream(f);
    }
}
