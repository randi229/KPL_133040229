/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul4.perform_any_string_modofication_before_validation;

/**
 *
 * @author Randi 133040229
 */
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
 
public class TagFilter {
    // Noncompliant code
//  public static String filterString(String str) {
//    String s = Normalizer.normalize(str, Form.NFKC);
// 
//    // Validate input
//    Pattern pattern = Pattern.compile("<script>");
//    Matcher matcher = pattern.matcher(s);
//    if (matcher.find()) {
//      throw new IllegalArgumentException("Invalid input");
//    }
// 
//    // Deletes noncharacter code points
//    s = s.replaceAll("[\\p{Cn}]", "");
//    return s;
//  }
// 
//  public static void main(String[] args) {
//    // "\uFDEF" is a noncharacter code point
//    String maliciousInput = "<scr" + "\uFDEF" + "ipt>";
//    String sb = filterString(maliciousInput);
//    // sb = "<script>"
//  }
    
    // Compliant code
    public static String filterString(String str) {
        String s = Normalizer.normalize(str, Form.NFKC);

        // Replaces all noncharacter code points with Unicode U+FFFD
        s = s.replaceAll("[\\p{Cn}]", "\uFFFD");

        // Validate input
        Pattern pattern = Pattern.compile("<script>");
        Matcher matcher = pattern.matcher(s);
        if (matcher.find()) {
            throw new IllegalArgumentException("Invalid input");
        }
        return s;
    }
    public static void main(String[] args) {
        // "\uFDEF" is a non-character code point
        String maliciousInput = "<scr" + "\uFDEF" + "ipt>";
        String s = filterString(maliciousInput);
        // s = <scr?ipt>
    }
}