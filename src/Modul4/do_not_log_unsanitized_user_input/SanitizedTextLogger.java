/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul4.do_not_log_unsanitized_user_input;

import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Randi 133040229
 */
class SanitizedTextLogger extends Logger {
  Logger delegate;
 
  public SanitizedTextLogger(Logger delegate) {
    super(delegate.getName(), delegate.getResourceBundleName());
    this.delegate = delegate;
  }
 
  public String sanitize(String msg) {
    Pattern newline = Pattern.compile("\n");
    Matcher matcher = newline.matcher(msg);
    return matcher.replaceAll("\n  ");
  }
 
  public void severe(String msg) {
    delegate.severe(sanitize(msg));
  }
 
  // .. Other Logger methods which must also sanitize their log messages
}
