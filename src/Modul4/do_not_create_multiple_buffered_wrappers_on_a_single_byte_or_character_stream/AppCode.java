/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul4.do_not_create_multiple_buffered_wrappers_on_a_single_byte_or_character_stream;

import java.io.BufferedInputStream;

/**
 *
 * @author Randi 133040229
 */
// Compliant code 2
class AppCode {

    private static BufferedInputStream in;

    AppCode() {
        in = InputLibrary.getBufferedWrapper();
    }
// ... Other methods
}