/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul4.distinguish_between_characters_or_bytes_read_from_a_stream_and_1;

import java.io.BufferedInputStream;
import java.io.FileInputStream;

/**
 *
 * @author Randi 133040229
 */
public class latihan {

    public static void main(String[] args) {
        // Noncompliant code
//        FileInputStream in;
//        // Initialize stream
//        byte data;
//        while ((data = (byte) in.read()) != -1) {
//            // ...
//        }
        
        // Compliat code
        FileInputStream in;
        // Initialize stream
        int inbuff;
        byte data;
        while ((inbuff = in.read()) != -1) {
        data = (byte) inbuff;
        // ...
}
    }
}
