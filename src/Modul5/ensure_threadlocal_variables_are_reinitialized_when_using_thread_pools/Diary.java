/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul5.ensure_threadlocal_variables_are_reinitialized_when_using_thread_pools;

/**
 *
 * @author Randi 133040229
 */
public final class Diary {

    public enum Day {

        MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY;
    }
    private static final ThreadLocal<Day> days = new ThreadLocal<Day>() {
        // Initialize to Monday
        protected Day initialValue() {
            return Day.MONDAY;
        }
    };

    private static Day currentDay() {
        return days.get();
    }

    public static void setDay(Day newDay) {
        days.set(newDay);
    }
// Performs some thread-specific task

    public void threadSpecificTask() {
// Do task ...
    }
    
    // Compliant code 1
    public static void removeDay() {
        days.remove();
    }
}
