/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul5.ensure_that_tasks_executing_in_a_thread_pool_do_not_fail_silently;

/**
 *
 * @author Randi 133040229
 */
final class Task implements Runnable {

    @Override
    public void run() {
        // ...
        throw new NullPointerException();
        // ...
    }
}
